# Generated by Django 4.0.6 on 2023-01-26 14:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0057_transmitterentry_unconfirmed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transmitterentry',
            name='citation',
            field=models.CharField(default='', help_text='A reference (preferrably URL) for this entry or edit', max_length=512),
        ),
    ]
